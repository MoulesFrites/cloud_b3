# Tp1 docker
## 2 "Création d'images"
Je créé un Dockerfile qui contiens :
```
FROM python:3-alpine
EXPOSE 8888
WORKDIR $HOME/
CMD python3 -m http.server 8888
```
Je le lance tel quel :
```
docker run -p 8888:8888 -d <conteneur_id>
```
Suite à ça, je rentre dans un navigateur `ip_vm:8000` et mon serveur python repond correctement

## 3 "Démon docker"
Dans le fichier docker.service on retrouve la ligne `Requires=docker.socket`

Dans le fichier docker.socket on peut trouver le path `ListenStream=/var/run/docker.sock`

Pour changer le socket on modifie ce block :
```
[Socket]
ListenStream=/var/run/docker.sock
SocketMode=0660
SocketUser=root
SocketGroup=docker
```
en ça :
```
[Socket]
ListenStream=2375
BindIPv6Only=both
Service=docker.service
```
On fais un p'tit `systemctl deamon-reload` et `systemctl restart docker`

Je verifie le status de docker
```
level=info msg="API listen on [::]:2375"
```
Donc ça marche, bah non evidemment :
```
[root@localhost ~]# docker ps
Cannot connect to the Docker daemon at unix:///var/run/docker.sock. Is the docker daemon running?
```
Et oui faut prevenir le "client" docker : `export DOCKER_HOST=tcp://127.0.0.1:2375`

Miracle !
```
[root@localhost ~]# docker ps
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
```
### Emplacement par défaut
Pour modifier l'emplacement par default des données docker, on modifie dans le fichier `docker.service` la ligne :
```
ExecStart= [...] --data-root /data/docker
```
Une fois fais, les conteneurs et autres ce stockerons dans /data/docker

### L'OOM (Out-Of Memory)

J'ai encore choisi de le modifier dans le `docker.service` mais petite subtilité, la doc docker nous assure que l'option :
```
--oom-score-adj
```
 fonctionne pour le daemon et un conteneur alors que dans le `docker.service` il faut bien écrire :
```
--oom-score-adjust
```
Sinon, et c'est ce que j'utilise, il suffit d'utiliser le paramètre systemd dans `[Service]`:
```
OOMScoreAdjust=<value>
```

## 4 "Docker compose"

Mes serveur web et autres services tournant actuellement sous docker-compose je n'ai pas trouvé pertinant de faire la "découverte" de docker-compose.

Mon infra correspond plus ou moins au tp, un mini apache par serveur web, un nginx qui fais le commutateur, un conteneur certbot pour les certs.

Le tout redondé sur un autre vps et un ptit script cloudflare qui fais le changement d'hote en 3sec si besoin.
