# Tp2

## `I. Gestion de conteneurs Docker`
Analysons docker et son fonctionnement ...

```
[root@localhost ~]# ps -ef | grep containerd
UID        PID  PPID  C  TTY CMD
root      1261     1  0  ?   /usr/bin/containerd
root      1262     1  0  ?   /usr/bin/dockerd [...]
```
La commande ci dessus nous montre que dockerd a pour père le pid 1 qui n'est autre que systemd :
```
/usr/lib/systemd/systemd --switched-root --system --deserialize 21
```
Le shim est lancé avec un conteneur donc si je lance un sleep :
```
UID        PID  PPID  C  TTY CMD
root      2898  1261  0  ?   containerd-shim [...]
```
Un process containerd-shim pop et il a pour parent containerd .

Pour utiliser l'api HTTP simplement : 

```
[root@localhost ~]# curl 127.0.0.1:2375/containers/json
```
On a un retour en json facilement exploitable derrière, ça me donne des idées de script ^^


Pour avoir les images en local on a juste à get `/images/json`

## `II. Sandboxing`

Pour recuperer les namespaces utilisé par mon shell, je commence par get le pid de mon bash `ps`, puis je ls -l le dossier `/proc/'pid'/ns/` :
```
[root@localhost ~]# ls -l /proc/2603/ns/
lrwxrwxrwx. 1 root root 0  7 janv. 13:38 ipc -> ipc:[4026531839]
lrwxrwxrwx. 1 root root 0  7 janv. 13:38 mnt -> mnt:[4026531840]
lrwxrwxrwx. 1 root root 0  7 janv. 13:38 net -> net:[4026531956]
lrwxrwxrwx. 1 root root 0  7 janv. 13:38 pid -> pid:[4026531836]
lrwxrwxrwx. 1 root root 0  7 janv. 13:38 user -> user:[4026531837]
lrwxrwxrwx. 1 root root 0  7 janv. 13:38 uts -> uts:[4026531838]
```
Je me retrouve donc avec 6 namespaces

Je liste ensuite tout les namespaces de mon hote, et je constate que mes 6 NS ont tous le pid 1 soit systemd et qu'il y'a un autre NS crée par kdevtmpfs

### `B. Unshare`

Je crée des namespaces mount, net et pid, la création d'un namespace user ne fonctionne pas et c'est normal puisque je suis sous centos 7, un patch et c'est enfin possible

```
[Je n'ai pas de nom !@localhost ~]$ lsns
        NS TYPE  NPROCS   PID USER  COMMAND
4026531838 uts        2  2764 65534 -bash
4026531839 ipc        2  2764 65534 -bash
4026532196 mnt        2  2764 65534 -bash
4026532198 net        2  2764 65534 -bash
4026532257 pid        2  2764 65534 -bash
4026532258 user       2  2764 65534 -bash
```
On peut voir que les NS uts et ipc sont les même que sur mon hote, par contre les autres ont changer donc c'est ok j'ai bien des nouveaux NS

### `C. Docker`

Si je demarre un conteneur docker je constate que 5 NS sont crée :
```
        NS TYPE  NPROCS   PID USER  COMMAND
4026532200 mnt        1  3104 root sleep 99999
4026532201 uts        1  3104 root sleep 99999
4026532202 ipc        1  3104 root sleep 99999
4026532203 pid        1  3104 root sleep 99999
4026532205 net        1  3104 root sleep 99999
```

Il n'y a pas de NS user de créé.

### `D. Nsenter`

Pour rentrer dans les NS de notre conteneur :
```
[root@localhost ~]# nsenter --target 3104 --mount --uts --ipc --net --pid /bin/sh
/ #
```

Pour verifier que je suis bien isolé, je reviens sur ma machine hote, je récupère le pid pour regarder les ns qui lui sont attribué :

```
[root@localhost ~]# ls -l /proc/3459/ns/
lrwxrwxrwx. 1 root root 0  7 janv. 16:49 ipc -> ipc:[4026532202]
lrwxrwxrwx. 1 root root 0  7 janv. 16:49 mnt -> mnt:[4026532200]
lrwxrwxrwx. 1 root root 0  7 janv. 16:49 net -> net:[4026532205]
lrwxrwxrwx. 1 root root 0  7 janv. 16:49 pid -> pid:[4026532203]
lrwxrwxrwx. 1 root root 0  7 janv. 16:49 user -> user:[4026531837]
lrwxrwxrwx. 1 root root 0  7 janv. 16:49 uts -> uts:[4026532201]
```

On peut remarquer que les NS sont tous different de ceux de mon hote sauf le user puisque je n'ai pas specifié à docker de le faire

### `E. User Namespaces`

Pour configurer docker avec les NS de type User j'ai utilisé ce tuto : https://docs.docker.com/engine/security/userns-remap/

Une fois tout fais on a bien un environnement complet avec un NS user généré au lancement d'un conteneur

```
        NS TYPE  NPROCS   PID USER   COMMAND
4026532184 user       1  2346 231072 bash
4026532185 mnt        1  2346 231072 bash
4026532186 uts        1  2346 231072 bash
4026532187 ipc        1  2346 231072 bash
4026532188 pid        1  2346 231072 bash
4026532190 net        1  2346 231072 bash
```
Cette liste de NS a été crée avec l'user testuser qui a pour id 321072

### `F. Isolation Reseau`

Je crée un conteneur simple :
`docker run -d -p 8888:7777 debian sleep 99999` <br/>
Dans mon conteneur : `eth0@if10 : "172.17.0.2"`<br>
Sur mon hote : `veth79ca94a@if9` sans ip et `docker0 : "172.17.0.1"` <br>
Docker à modifié les règles iptables pour ce conteneur :
```
Chain DOCKER (1 references)
target     prot opt source               destination         
ACCEPT     tcp  --  anywhere             172.17.0.2           tcp dpt:cbt
```
## `2. Cgroups`

### `A. Découverte manuelle`

Je lance un conteneur et je go `cat /proc/<PID>/cgroup`<br>
Ce qui me renvoi : 

```
11:blkio:/system.slice/docker.service
10:perf_event:/
9:freezer:/
8:pids:/system.slice/docker.service
7:cpuacct,cpu:/system.slice/docker.service
6:memory:/system.slice/docker.service
5:devices:/system.slice/docker.service
4:cpuset:/
3:hugetlb:/
2:net_prio,net_cls:/
1:name=systemd:/system.slice/docker.service
```

### `B. Utilisation par Docker`

Je lance un conteneur et je pars a l'aventure dans `/sys/fs/cgroup`<br>

Pour la ram je vais dans `./memory/docker/<id>/memory.limit_in_bytes` : `9223372036854771712`<br>
Pour le nombre de processus `pids/docker/<id>/pids.max` : `max`<br>
Pour le nombre de cpu alloué `pids/docker/<id>/cpuset.cpus` : `0`<br>

Je vais tenter de changer ces valeurs avec des option de `Docker run`<br>


1er test : `docker run -m 123456789 -d debian sleep 99999`<br>
Ram `./memory/docker/<id>/memory.limit_in_bytes` : `123453440`

Il n'est pas possible de changer le nombre de process via les option docker run

Le nombre de cpu alloué découle directement de son parent, lui meme à 0, il est donc impossible de changer ce dernier
```
docker: Error response from daemon: Requested CPUs are not available - requested 0-2, available: 0.
```

## `3. Capabilities`


### `A. Découverte manuelle`